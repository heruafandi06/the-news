import Vuex from 'vuex';

import news from './modules/news';

const createStore = () => {
    return new Vuex.Store({
        modules: {
            news
        },
    });
};

export default createStore;
