import axios from 'axios';

export const state = () => ({
    data: []
})

export const getters = {
    getAllData: (state) => state.data
}

export const actions = {
    async fetchData({ commit }) {
        let uri_end_point = 'https://www.spaceflightnewsapi.net/api/v2/articles'
        const res = await axios.get(uri_end_point)
        // console.log(res)
        commit('addData', res.data)
    },
    async fetchDataById({ commit }, id) {
        let uri_end_point = 'https://www.spaceflightnewsapi.net/api/v2/articles/' + id
        const res = await axios.get(uri_end_point)
        // console.log(res)
        commit('addData', res.data)
    },
};

export const mutations = {
    addData: (state, data) => state.data = data
};

export default {
    state,
    getters,
    actions,
    mutations
}
